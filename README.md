# Docker images with Kaniko for GitLab CI on Fargate

Alpine and Ubuntu-based Docker images ready to run
[Kaniko](https://github.com/GoogleContainerTools/kaniko) CI jobs with the [AWS
Fargate Custom Executor driver](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate)
for [GitLab Runner](https://docs.gitlab.com/runner).

[![pipeline status](https://gitlab.com/aws-fargate-driver-demo/docker-kaniko-gitlab-ci-fargate/badges/master/pipeline.svg)](https://gitlab.com/aws-fargate-driver-demo/docker-kaniko-gitlab-ci-fargate/-/commits/master)

**WORK IN PROGRESS!** This solution is not fully operational yet (please visit Fargate driver
[MR!34](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate/-/merge_requests/34)
for further details).
